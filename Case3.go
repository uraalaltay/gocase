func topKFrequency(words []string, k int) []string {
    frequency := [][]int{}
    res := make([]string, k)

    frequency = addWordsToFrequency(words, frequency)
  
    sortFrequency(words, frequency)

    for i := len(frequency) - 1; i >= len(frequency)-k; i-- {
        res[len(frequency) - i - 1] = words[frequency[i][0]]
    }
		//  the part where word is the key and frequency is the value 
    return res
}