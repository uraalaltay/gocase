//For this case I was hesitating using either sort.Slice() , sort.SliceStable() and after doing some research,
//I ve decided to apply for Sort.Slice()
func main() {
arr  := []string{"aaaasd", "a", "aab", "aaabcd", "ef", "cssssssd", "fdz", "kf", "zc", "lklklklklklklklkl", "l"}

//the input piece is big and to get to length of a string use len() returns the UTF-8 encoded byte length, not the number of runes

sort.Slice(arr , func(i, j int) bool {
    s1, s2 := arr [i], arr [j]
    count1, count2 := strings.Count(s1, "a"), strings.Count(s2, "a")
    if count1 != count2 {
        return count1 > count2
    }
    return utf8.RuneCountInString(s1) > utf8.RuneCountInString(s2)
})

fmt.Println(arr)

}

// output : [aaaasd aaabcd aab a lklklklklklklklkl cssssssd fdz ef kf zc l]
 
