//variable identifier declared inside a function begins at the end of the ConstSpec or VarSpec 
//also ; (ShortVarDecl for short variable declarations) 

func TestFunction(n int) int {
	a := 10
	var Test2 func(m int) int

	Test2 = func(m int) int {
		if m <= a {
			return a
		}
		return Test2(m - 1)
	}

	return Test2(n)
}

func main() {
	fmt.Println(TestFunction(15))
}
